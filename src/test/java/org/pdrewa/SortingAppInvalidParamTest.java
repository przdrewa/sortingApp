package org.pdrewa;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

/**
 * Unit tests for Sorting AppTest with invalid parameters
 */
public class SortingAppInvalidParamTest {
    private ByteArrayOutputStream sink;
    private PrintStream controlledOut;
    private ByteArrayInputStream controlledIn;
    private PrintStream defaultOut;
    private InputStream defaultIn;

    @Test(expected = IllegalArgumentException.class)
    public void testNoParam() {

        setControlledStreamsWithInput("\n");
        try {
            SortingApp.main(new String[]{});
            controlledOut.flush();

        } finally {
            setStandardStreams();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParamOnly1Space() {

        setControlledStreamsWithInput(" ");
        try {
            SortingApp.main(new String[]{});
            controlledOut.flush();
        } finally {
            setStandardStreams();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParamOnly3Space() {
        setControlledStreamsWithInput("   ");
        try {
            SortingApp.main(new String[]{});
            controlledOut.flush();

        } finally {
            setStandardStreams();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParamMoreThan10() {
        setControlledStreamsWithInput("10 9 8 7 6 5 4 3 2 1 0");
        try {
            SortingApp.main(new String[]{});
            controlledOut.flush();
        } finally {
            setStandardStreams();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParamNotANumber() {
        setControlledStreamsWithInput("2 9 8 7 F 5");
        try {
            SortingApp.main(new String[]{});
            controlledOut.flush();
        } finally {
            setStandardStreams();
        }
    }

    private void setControlledStreamsWithInput(final String input) {

        sink = new ByteArrayOutputStream();
        controlledOut = new PrintStream(sink);
        controlledIn = new ByteArrayInputStream(input.getBytes());

        defaultOut = System.out;
        defaultIn = System.in;

        System.setOut(controlledOut);
        System.setIn(controlledIn);
    }

    private void setStandardStreams() {
        System.setOut(defaultOut);
        System.setIn(defaultIn);
    }
}
