package org.pdrewa;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Unit tests for Sorting AppTest with correct parameters
 */
class SortingAppCorrectParamTest {
    private ByteArrayOutputStream sink;
    private PrintStream controlledOut;
    private ByteArrayInputStream controlledIn;
    private PrintStream defaultOut;
    private InputStream defaultIn;

    static Stream<Arguments> paramsCorrectCases() throws IOException {
        Iterator<String> parameters = Files.readAllLines(Paths.get("src/test/resources/sortingAppCorrectParams.txt")).iterator();
        Stream<String> target = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(parameters, Spliterator.ORDERED), false);
        return target.map(q -> {
                    String[] splittedLineFromFile = q.split(";");
                    return Arguments.of(splittedLineFromFile[0], splittedLineFromFile[1]);
                }
        );
    }

    @ParameterizedTest
    @MethodSource("paramsCorrectCases")
    void testCorrectParams(final String input, final String expected) {
        setControlledStreamsWithInput(input);
        try {
            SortingApp.main(new String[]{});
            controlledOut.flush();
            assertEquals(expected, sink.toString().trim(), "Error on input " + input);
        } finally {
            setStandardStreams();
        }
    }

    private void setControlledStreamsWithInput(final String input) {

        sink = new ByteArrayOutputStream();
        controlledOut = new PrintStream(sink);
        controlledIn = new ByteArrayInputStream(input.getBytes());

        defaultOut = System.out;
        defaultIn = System.in;

        System.setOut(controlledOut);
        System.setIn(controlledIn);
    }

    private void setStandardStreams() {
        System.setOut(defaultOut);
        System.setIn(defaultIn);
    }

}
