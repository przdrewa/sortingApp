package org.pdrewa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Please enter 1 to 10 integers separated by a space
 * i.e. "10 9 8 -100 0"
 */
public class SortingApp {

    public static void main(String[] args) {

        String userInput = getInputFromUser();
        List<Integer> userNumbers = convertUserInputToList(userInput);
        sort(userNumbers);
        print(userNumbers);
    }

    public static void sort(List<Integer> numbers) {
        if (numbers == null || numbers.size() > 10) {
            throw new IllegalArgumentException();
        }
        Collections.sort(numbers);
    }

    /**
     * Gets the input from user. Proper format are up to ten integers separated by space i.e. 10 9 8 -100
     *
     * @return input from user
     */
    public static String getInputFromUser() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public static List<Integer> convertUserInputToList(String userInput) {
        List<Integer> userNumbers = new ArrayList<>();
        String[] inputArray = userInput.split(" +");

        if (inputArray.length == 0 || inputArray.length > 10 || inputArray[0].equals("")) {
            throw new IllegalArgumentException("Wrong size of list. Start application again" +
                    " and make sure that there is at least 1 argument and no more than 10");
        }

        try {
            for (String element : inputArray)
                userNumbers.add(Integer.parseInt(element));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Illegal type of argument. Start application again and make sure to type only numbers");
        }

        return userNumbers;
    }

    public static void print(List<Integer> numbers) {
        for (Integer number : numbers) {
            System.out.print(number + " ");
        }
    }
}


